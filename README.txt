There are accessibility and usability issues with CAPTCHA; However, we all hate
Spam. This service provides phone-based CAPTCHAs, using an API provided by 
phonewithcomputer.com.

All that is needed is a API key and account ID, available at:

http://phonewithcomputer.com/user

INSTALLATION
============

1) You can apply an account at http://phonewithcomputer.com/user/register

2) You will receive an email afterward, check spam folder if you do not
receive the E-mail in 15 minutes.

3) Use the password in the received E-mail and login at http://phonewithcomputer.com/user

4) Configure your drupal site at admin/config/people/captcha/phone_captcha with
the your Api Key ID at http://phonewithcomputer.com/myaccount and secure key at
http://phonewithcomputer.com/user .

5) Choose a plan which fit your need at http://phonewithcomputer.com/user.

6) The rest of configurations are same as other of captcha.
